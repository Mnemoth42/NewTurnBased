using System;
using System.Linq;
using UnityEngine;

namespace TrottersWorkshop
{
    [CreateAssetMenu(fileName = "Game Constants", menuName = "Utilities/GameConstants")]
    public class GameConstants : ScriptableObject
    {
        
        #region fields
        [SerializeField] private float cellWidth = 2.0f;
        [SerializeField] private float cellDepth = 2.0f;
        [SerializeField] private float stepHeight = .5f;
        #endregion

        #region properties
        public float CellWidth => cellWidth;
        public float CellDepth => cellDepth;
        public float StepHeight => stepHeight;
        #endregion
        
        private static GameConstants instance;
        public static GameConstants Instance
        {
            get
            {
                if (!instance)
                {
                    instance = Resources.LoadAll<GameConstants>("").First();
                }

                if (!instance)
                {
                    throw new Exception("No instance of GameConstants was found in a Resources folder!  " +
                                        "Please ensure exactly one GameConstants ScriptableObject exists within" +
                                        "a resource folder.");
                }
                return instance;
            }
        }

        
    }
}
