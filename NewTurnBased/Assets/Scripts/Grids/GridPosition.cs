﻿using System;
using UnityEngine;

namespace TrottersWorkshop.Grids
{
    public struct GridPosition
    {
        public int x;
        public int y;
        public int z;

        public GridPosition(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        
        public override int GetHashCode()
        {
            return HashCode.Combine(x, y, z);
        }

        public override bool Equals(object obj)
        {
            if (obj is GridPosition other)
            {
                return x == other.x && y == other.y && z == other.z;
            }
            return false;
        }

        public override string ToString()
        {
            return $"[{x},{y},{z}]";
        }

        public static GridPosition operator +(GridPosition a, GridPosition b)
        {
            return new GridPosition(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        public static GridPosition operator -(GridPosition a, GridPosition b)
        {
            return new GridPosition(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        /// <summary>
        /// Distance between two GridPositions where movement is restricted to Cardinal (N,S,E,W) directions.
        /// Excludes height from calculation.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int CardinalDistance(GridPosition a, GridPosition b)
        {
            return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.z - b.z);
        }

        /// <summary>
        /// Distance between two GridPositions where movement cost is equal for movement in any directions (Cardinal +
        /// Diagonal directions) Excludes height (y) from calculation.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int KingsDistance(GridPosition a, GridPosition b)
        {
            int distX = Mathf.Abs(a.x - b.x);
            int distZ = Mathf.Abs(a.z - b.z);
            if (distX > distZ)
            {
                (distX, distZ) = (distZ, distX);
            }
            return distX + (distZ - distX);
        }

        /// <summary>
        /// Distance between two GridPositions using a movement cost of 1 for Cardinal directions and 1.4 for Diagonal
        /// directions.  Excludes height (y) from calculation.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static float Distance(GridPosition a, GridPosition b)
        {
            int distX = Mathf.Abs(a.x - b.x);
            int distZ = Mathf.Abs(a.z - b.z);
            if (distX > distZ)
            {
                (distX, distZ) = (distZ, distX);
            }

            return distX * 1.4f + (distZ-distX);
        }
        
        
    }
}